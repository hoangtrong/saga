/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import SagaComponent from './components/SagaComponent';
import SagaContainer from './redux/containers/SagaContainer';
import { Provider } from 'react-redux';
import Store from './redux/store/Store';

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <SagaContainer/>
      </Provider>
    );
  }
}
