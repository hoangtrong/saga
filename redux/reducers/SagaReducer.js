import ActionTypes from '../actions/ActionTypes';

const SagaReducer = (array = [], action) => {
    switch(action.type) {
        case ActionTypes.GET_SUCCEED: return action.array;
        case ActionTypes.GET_FAIL: return [];
        default: return array;
    }
}

export default SagaReducer;