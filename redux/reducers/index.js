import SagaReducer from './SagaReducer';
import { combineReducers } from 'redux';

const reducers = combineReducers({ SagaReducer });

export default reducers;