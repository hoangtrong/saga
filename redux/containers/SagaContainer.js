import { connect } from 'react-redux';
import SagaComponent from '../../components/SagaComponent';
import { getAPIAction } from "../actions/index";

const mapStateToProps = (state) => {
    return {
        array: state.SagaReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetAPI : () => {
            dispatch(getAPIAction())
        }
    };
}

const SagaContainer = connect(mapStateToProps, mapDispatchToProps)(SagaComponent);

export default SagaContainer;