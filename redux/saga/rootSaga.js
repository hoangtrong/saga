import { call, all } from 'redux-saga/effects';
import { watchGetApi } from './saga';

export default function* rootSaga() {
     yield call(watchGetApi)
}