import ActionTypes from '../actions/ActionTypes';
import { put, takeLatest, takeEvery } from 'redux-saga/effects';
import { Api } from './Api';

function* getApi() {
    try {
        const array = yield Api.getAPi();
        yield put({ type: ActionTypes.GET_SUCCEED, array })

    } catch (error) {
        yield put({ type: ActionTypes.GET_FAIL, error })
    }
}

function* test() {
    alert('hello')
}

export function* watchGetApi() {
    yield takeLatest(ActionTypes.GET_API, getApi);
}