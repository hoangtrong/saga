const url = 'https://api.coinmarketcap.com/v1/ticker/?start=0&limit=10';

function* getAPi() {
    const response = yield fetch( url );
    const array = yield response.json();
    return array;
}

export const Api = {
    getAPi
}

