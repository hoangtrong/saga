import ActionTypes from './ActionTypes';

const getAPIAction = (value) => {
    return {
        type: ActionTypes.GET_API,
        value
    }
}

const getAPISucceedAction = (array) => {
    return {
        type: ActionTypes.GET_SUCCEED,
        array
    }
};

const getAPIFailAction = (error) => {
    return {
        type: ActionTypes.GET_FAIL,
        error
    }
}

export {
    getAPIFailAction,
    getAPISucceedAction,
    getAPIAction
}