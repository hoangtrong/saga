const ActionTypes = {
    GET_API: 'GET_API',
    GET_SUCCEED: 'GET_SUCCEED',
    GET_FAIL: 'GET_FAIL'
}

export default ActionTypes;