import React, { PureComponent } from 'react';
import { View, FlatList, TouchableOpacity, Text } from 'react-native';
const data = [
    {
        id: 1,
        name: 'hello'
    }
]
var array;
const url = 'https://api.coinmarketcap.com/v1/ticker/?start=0&limit=10';

class SagaComponent extends PureComponent {
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                    <TouchableOpacity 
                    onPress={()=>{ this.props.onGetAPI()  }}
                    style={{ padding: 15, backgroundColor: 'blue',borderRadius: 5 }}>
                        <Text style={{ fontSize: 16, }}>GET API</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.props.array}
                    style={{ flex: 3 }}
                    renderItem={this.renderItem}
                    keyExtractor={item=>item.id}
                />
            </View>
        );
    }

    renderItem = ({ item }) => (
        <Text style={{ textAlign: 'center', width: '100%' }}>{item.name}</Text>
    )
}

export default SagaComponent;